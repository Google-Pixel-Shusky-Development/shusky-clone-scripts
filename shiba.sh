#!/bin/bash 
FILE=bootstrap.bash
if [ -f "$FILE" ]; then
    echo "Deleting all Google Pixel 8 directories!"

rm -rf device/google/shusky
rm -rf device/google/shusky-kernel
rm -rf device/google/zuma
rm -rf device/google/gs-common
rm -rf device/google/gs101
rm -rf kernel/google/shusky/shiba
rm -rf hardware/google
rm -rf vendor/google/shiba 

echo "Cloning everything for Shiba (Google Pixel 8)"

git clone https://gitlab.com/Google-Pixel-Shusky-Development/android_device_google_gs-common device/google/gs-common 
git clone https://gitlab.com/Google-Pixel-Shusky-Development/android_device_google_gs101 device/google/gs101
git clone https://gitlab.com/Google-Pixel-Shusky-Development/android_device_google_shusky device/google/shusky
git clone https://gitlab.com/Google-Pixel-Shusky-Development/android_device_google_zuma device/google/zuma
git clone --depth=1 https://android.googlesource.com/device/google/shusky-kernel -b android-14.0.0_r17 device/google/shusky-kernel
git clone https://github.com/HurtCopain/Kirisakura_Pantah kernel/google/susky/shiba
git clone https://android.googlesource.com/platform/hardware/google/aemu -b android-14.0.0_r17 hardware/google/aemu
git clone https://android.googlesource.com/platform/hardware/google/apf -b android-14.0.0_r17 hardware/google/apf
git clone https://android.googlesource.com/platform/hardware/google/av -b android-14.0.0_r17 hardware/google/av
git clone https://android.googlesource.com/platform/hardware/google/gchips -b android-14.0.0_r17 hardware/google/gchips
git clone https://android.googlesource.com/platform/hardware/google/graphics/common -b android-14.0.0_r17 hardware/google/graphics/common
git clone https://android.googlesource.com/platform/hardware/google/graphics/gs101 -b android-14.0.0_r17 hardware/google/graphics/gs101
git clone https://android.googlesource.com/platform/hardware/google/graphics/gs201 -b android-14.0.0_r17 hardware/google/graphics/gs201
git clone https://android.googlesource.com/platform/hardware/google/graphics/zuma -b android-14.0.0_r17 hardware/google/graphics/zuma
git clone https://android.googlesource.com/platform/hardware/google/pixel -b android-14.0.0_r17 hardware/google/pixel
git clone https://android.googlesource.com/platform/hardware/google/pixel-sepolicy -b android-14.0.0_r17 hardware/google/pixel-sepolicy
git clone https://github.com/TheMuppets/proprietary_vendor_google_shiba vendor/google/shiba
else 
    echo "Not in the root of an Android ROM! Please go into the root, grab shiba.sh, and run it in the root of the ROM directory!"
fi
